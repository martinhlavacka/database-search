<!-- FORM -->
<form action="" method="post">

	<!-- SEARCH PHRASE -->
	<input name="phrase" type="text" placeholder="Search for ...">

	<!-- SEARCH -->
	<button type="submit">Search</button>
	
</form> 

<?php 	
	
	/* ---------------------------
	POST REQUEST
	--------------------------- */

	if(isset($_POST['phrase'])){

		/* ---------------------------
		DATABASE CONFIGURATION
		--------------------------- */

		// DATABASE CONNECTION VALUES
		$database_type	= 'mysql';
		$database_name 	= 'book';
		$server 		= 'localhost';
		$username 		= 'root';
		$password 		= 'password';

		// BRING DATABSE
		require_once("medoo.php");

		/* ---------------------------
		DATABASE DUMP
		--------------------------- */

		// GET DATA FroM DATABASE
		require_once("dump.php");

		/* ---------------------------
		GET DUMP CONTENT TO WORK WITH
		--------------------------- */
		
		// KEYWORD
		$keyword = $_POST['phrase'];

		// LOAD FILE WITH SQL
		$content = file_get_contents ("backups/" . $databaseBackup->backup_name . ".sql");

		// REMOVE THE DUMP
		unlink("backups/" . $databaseBackup->backup_name . ".sql");

		/* ---------------------------
		DISPLAY DATABASE LOCATIONS
		--------------------------- */

		// SHOW SEARCH PHRASE
		echo "<b>Search phrase:</b> " .  $_POST['phrase'] . "<br><br>";

		// TITLE
		echo "<b>Found at following locations:</b>";

		// MAKE ORDERED LIST
		echo "<ol>";

		// LOOP THROUGH LINES
		foreach(preg_split("/((\r?\n)|(\r\n?))/", $content) as $line){
		    
		    // IF THE LINE INCLUDES KEYWORD
		    if (strpos($line, $keyword) !== false) {

		    	// GET LINE
				$line = str_replace("INSERT INTO `", "", $line);

				// SHOW TABLE NAME
				$table_name = strstr($line, '`', true);

				// STORE DATA FOR EACH OF THE COLUMNS IN CURRENT DATABASE
				$datas = $database->select("{$table_name}", '*');

				// DISPLAY ITEM
				foreach ($datas as $datakey => $datavalue) {
					foreach ($datavalue as $keyone => $valueone) {
						if(strstr($valueone, $keyword , true)){
							echo "<li>";
								echo $table_name;
								echo ".";
								echo $keyone;
							echo "</li>";
						}
					}
				}

				// NEW LINE
				echo "<br>";
			}

		} 

		// CLOSE LIST
		echo "</ol>";

	}
	
?>